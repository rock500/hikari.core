#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © Nekokatt 2019-2020
#
# This file is part of Hikari.
#
# Hikari is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hikari is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Hikari. If not, see <https://www.gnu.org/licenses/>.
"""
Hikari's core framework for writing Discord bots in Python.
"""


from hikari import client
from hikari import errors
from hikari import internal_utilities
from hikari import net
from hikari import orm
from hikari.orm import gateway
from hikari.orm.state import base_registry

__author__ = "Nekokatt"
__contributors__ = {"FasterSpeeding", "LunarCoffee", "raatty", "Tmpod", "Zach", "thomm.o", "rock500", "davfsa"}
__copyright__ = f"© 2019-2020 Nekokatt"
__license__ = "LGPLv3"
__version__ = "0.0.65"
__url__ = "https://gitlab.com/nekokatt/hikari"
